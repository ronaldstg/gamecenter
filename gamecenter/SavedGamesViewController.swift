//
//  SavedGamesViewController.swift
//  gamecenter
//
//  Created by Ronald de Souza on 04/03/18.
//  Copyright © 2018 Ronald de Souza. All rights reserved.
//

import UIKit
import CoreData
import SwipeCellKit

class SavedGamesViewController: UIViewController {

    var navBar: UINavigationBar!
    var tableView = UITableView()
    var gameItems: [NSManagedObject] = []
    var selectedIndex:Int!
    
    let sortView = UIView()
    let sortLabel = UILabel()
    var isSortByAscending:Bool = true
    
    let totalView = UIView()
    let totalLabel = UILabel()
    var total:Float = 0.0
    
    
    override func viewWillAppear(_ animated: Bool) {
        getDataSortedBy(sortByAscending: self.isSortByAscending)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        placeNavBar()
        placeSortMenu()
        placeTableView()
        placeFooter()
    }
    
    func placeNavBar() {
        navBar = UINavigationBar(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 44))
        self.view.addSubview(navBar);
        let navItem = UINavigationItem(title: "GAMECENTER - MEUS GAMES");
        
        let backItem = UIBarButtonItem(title: "Voltar", style: .plain, target: self, action: #selector(backTapped))
        backItem.tintColor = .black
        navItem.leftBarButtonItem = backItem
        navBar.setItems([navItem], animated: false);
    }
    
    func placeSortMenu() {
        self.view.addSubview(sortView)
        sortView.isUserInteractionEnabled = true
        sortView.backgroundColor = UIColor(red:0.31, green:0.47, blue:0.69, alpha:1.0)
        sortView.addSubview(sortLabel)
        sortView.snp.makeConstraints { (make) in
            make.top.equalTo(self.navBar.frame.height)
            make.height.equalTo(40)
            make.left.equalTo(0)
            make.right.equalTo(0)
        }
        
        sortLabel.text = "Ordenando por: Mais qualificado"
        sortLabel.textColor = UIColor.white
        sortLabel.textAlignment = .center
        sortLabel.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.height.equalTo(40)
            make.top.equalTo(0)
        }
        
        sortView.onTap { _ in
            self.total = 0
            self.isSortByAscending = !self.isSortByAscending
            self.getDataSortedBy(sortByAscending: self.isSortByAscending)
            self.tableView.reloadData()

            
            if (self.isSortByAscending) {
                self.sortLabel.text = "Ordenando por: Mais qualificado"
            } else {
                self.sortLabel.text = "Ordenando por: Menos qualificado"
            }
            
        }
    }
    
    func placeTableView() {
        
        tableView.frame = CGRect(x: 0, y: 84, width: view.frame.width, height: view.frame.height - 200)
        tableView.delegate   = self
        tableView.dataSource = self
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 45
        
        tableView.register(SwipeTableViewCell.self as AnyClass, forCellReuseIdentifier: "Cell")
        
        self.view.addSubview(tableView)
    }
    
    func placeFooter() {
    
        self.view.addSubview(totalView)
        totalView.addSubview(totalLabel)
        totalView.snp.makeConstraints { (make) in
            make.top.equalTo(self.tableView.frame.height + 100)
            make.height.equalTo(80)
            make.left.equalTo(0)
            make.right.equalTo(0)
        }
        
        totalLabel.text = "Valor total: \(self.total)"
        totalLabel.textAlignment = .right
        totalLabel.font = UIFont.boldSystemFont(ofSize: 20.0)
        totalLabel.snp.makeConstraints { (make) in
            make.top.equalTo(20)
            make.left.equalTo(10)
            make.right.equalTo(-10)
            make.height.equalTo(20)
        }
    }
    
    func getDataSortedBy(sortByAscending:Bool) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<GameItem>(entityName: "GameItem")
        let sort = NSSortDescriptor(key: #keyPath(GameItem.rating), ascending: sortByAscending)
        fetchRequest.sortDescriptors = [sort]
        do {
            gameItems = try managedContext.fetch(fetchRequest)
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    @objc func backTapped(sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
}


// MARK: - UITableViewDataSource
extension SavedGamesViewController: UITableViewDelegate, UITableViewDataSource, SwipeTableViewCellDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return gameItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! SwipeTableViewCell
        let index = indexPath.row as Int
        let feedItem = gameItems[index]
        
        cell.delegate = self
        cell.textLabel?.text = feedItem.value(forKeyPath: "title") as? String
        
        self.total = self.total + (feedItem.value(forKeyPath: "price") as? Float)!
        self.totalLabel.text = "Valor total: " + String(format:"%.2f", self.total)

        
        return cell
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        
        guard orientation == .right else { return nil }
        let index = indexPath.row as Int
        
        let deleteAction = SwipeAction(style: .destructive, title: "Apagar") { action, indexPath in
            self.deleteRow(itemIndex:index)
        }
        
        return [deleteAction]
    }
    
    func deleteRow(itemIndex:Int) {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        let objectToDelete = self.gameItems[itemIndex]
        self.total = 0
        
        managedContext.delete(objectToDelete)
        
        self.gameItems.remove(at: itemIndex)
        self.tableView.reloadData {
            self.totalLabel.text = "Valor total: " + String(format:"%.2f", self.total)
        }
        
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
}

extension UITableView {
    func reloadData(completion: @escaping ()->()) {
        UIView.animate(withDuration: 0, animations: { self.reloadData() })
        { _ in completion() }
    }
}






