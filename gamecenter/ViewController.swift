//
//  ViewController.swift
//  gamecenter
//
//  Created by Ronald de Souza on 04/03/18.
//  Copyright © 2018 Ronald de Souza. All rights reserved.
//

import UIKit
import SnapKit
import Cosmos
import CoreData
import GestureRecognizerClosures
import SwiftValidator

class ViewController: UIViewController, ValidationDelegate {
    
    var navBar: UINavigationBar!
    var nameTextField = LeftPaddedTextField()
    var platformTextField = LeftPaddedTextField()
    var kindTextField = LeftPaddedTextField()
    var priceTextField = LeftPaddedTextField()
    var ratingView = CosmosView()
    
    var saveButtonLabel = UILabel()
    var saveButtonView = UIView()
    
    var gameItems: [NSManagedObject] = []
    
    let background:UIImageView = UIImageView(image: UIImage(named:"bg"))
    
    let validator = Validator()

    override func viewDidLoad() {
        super.viewDidLoad()
        let saveTap = UITapGestureRecognizer(target: self, action: Selector(("handleSaveTap:")))
        saveTap.numberOfTapsRequired = 1
        saveTap.numberOfTouchesRequired = 1
        //saveTap.delegate = (self as! UIGestureRecognizerDelegate)
        
        view.addSubview(background)
        placeNavBar()
        view.addSubview(nameTextField)
        view.addSubview(platformTextField)
        view.addSubview(kindTextField)
        view.addSubview(priceTextField)
        view.addSubview(ratingView)
        view.addSubview(saveButtonView)
        
        validator.registerField(nameTextField, rules: [RequiredRule()])
        validator.registerField(platformTextField, rules: [RequiredRule()])
        validator.registerField(kindTextField, rules: [RequiredRule()])
        validator.registerField(priceTextField, rules: [RequiredRule()])
        
        background.snp.makeConstraints { (make) in
            make.top.equalTo(0)
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.bottom.equalTo(0)
        }
        
        nameTextField.placeholder = "Nome do Jogo"
        nameTextField.layer.borderColor = UIColor(red:0.31, green:0.47, blue:0.69, alpha:1.0).cgColor
        nameTextField.layer.borderWidth = 1
        nameTextField.snp.makeConstraints { (make) in
            make.top.equalTo(navBar.frame.height + 30)
            make.left.equalTo(20)
            make.right.equalTo(-20)
            make.height.equalTo(40)
        }
        
        platformTextField.placeholder = "Plataforma"
        platformTextField.layer.borderColor = UIColor(red:0.31, green:0.47, blue:0.69, alpha:1.0).cgColor
        platformTextField.layer.borderWidth = 1
        platformTextField.snp.makeConstraints { (make) in
            make.top.equalTo(nameTextField.snp.bottom).offset(10)
            make.left.equalTo(20)
            make.right.equalTo(-20)
            make.height.equalTo(40)
        }
        
        kindTextField.placeholder = "Gênero"
        kindTextField.layer.borderColor = UIColor(red:0.31, green:0.47, blue:0.69, alpha:1.0).cgColor
        kindTextField.layer.borderWidth = 1
        kindTextField.snp.makeConstraints { (make) in
            make.top.equalTo(platformTextField.snp.bottom).offset(10)
            make.left.equalTo(20)
            make.right.equalTo(-20)
            make.height.equalTo(40)
        }
        
        priceTextField.placeholder = "Preço estimado"
        priceTextField.layer.borderColor = UIColor(red:0.31, green:0.47, blue:0.69, alpha:1.0).cgColor
        priceTextField.layer.borderWidth = 1
        priceTextField.snp.makeConstraints { (make) in
            make.top.equalTo(kindTextField.snp.bottom).offset(10)
            make.left.equalTo(20)
            make.right.equalTo(-20)
            make.height.equalTo(40)
        }
        
        ratingView.rating = 1
        ratingView.text = "Classificação"
        ratingView.settings.updateOnTouch = true
        
        // Show only fully filled stars
        ratingView.settings.fillMode = .full
        
        // Change the size of the stars
        ratingView.settings.starSize = 30
        
        // Set the distance between stars
        ratingView.settings.starMargin = 5
        
        // Set the color of a filled star
        ratingView.settings.filledColor = UIColor(red:0.31, green:0.47, blue:0.69, alpha:1.0)
        // Set the border color of an empty star
        ratingView.settings.emptyBorderColor = UIColor(red:0.31, green:0.47, blue:0.69, alpha:1.0)
        // Set the border color of a filled star
        ratingView.settings.filledBorderColor = UIColor(red:0.31, green:0.47, blue:0.69, alpha:1.0)
        ratingView.snp.makeConstraints { (make) in
            make.top.equalTo(priceTextField.snp.bottom).offset(10)
            make.left.equalTo(20)
            make.right.equalTo(-20)
            make.height.equalTo(40)
        }
        
        // Register touch handlers
        //ratingView.didTouchCosmos = didTouchCosmos
        //ratingView.didFinishTouchingCosmos = didFinishTouchingCosmos
       
        saveButtonView.addSubview(saveButtonLabel)
        saveButtonView.isUserInteractionEnabled = true
        saveButtonView.layer.cornerRadius = 10
        saveButtonView.backgroundColor = UIColor(red:0.31, green:0.47, blue:0.69, alpha:1.0)
        saveButtonView.snp.makeConstraints { (make) in
            make.top.equalTo(ratingView.snp.bottom).offset(40)
            make.left.equalTo(20)
            make.right.equalTo(-20)
            make.height.equalTo(50)
        }
        
        saveButtonView.onTap { _ in
            self.validator.validate(self)
        }
        
        saveButtonLabel.text = "Salvar"
        saveButtonLabel.textAlignment = .center
        saveButtonLabel.textColor = UIColor.white
        saveButtonLabel.snp.makeConstraints { (make) in
            make.top.equalTo(5)
            make.left.equalTo(20)
            make.right.equalTo(-20)
            make.height.equalTo(40)
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func placeNavBar() {
        
        UIApplication.shared.isStatusBarHidden = true
        
        navBar = UINavigationBar(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 44))
        self.view.addSubview(navBar)
        let navItem = UINavigationItem(title: "GAMECENTER");
        
        let btnFavorites = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        btnFavorites.layer.masksToBounds = true
        btnFavorites.setImage(UIImage(named: "save_icon"), for: .normal)
        btnFavorites.imageEdgeInsets = UIEdgeInsetsMake(5, 10, 5, 5)
        btnFavorites.addTarget(self, action: #selector(pressButton(button:)), for: .touchUpInside)
        
        let exitItem = UIBarButtonItem(title: "Sair", style: .plain, target: self, action: #selector(exitTapped))
        exitItem.tintColor = .black
        navItem.leftBarButtonItem = exitItem
        
        let rightBarButton = UIBarButtonItem(customView: btnFavorites)
        navItem.rightBarButtonItem = rightBarButton
        navBar.setItems([navItem], animated: false);
    }
    
    @objc func exitTapped(sender: UIBarButtonItem) {
        
        let alertController = UIAlertController(title: "Deseja sair", message: "Deseja sair do Game Center?", preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Sair", style: UIAlertActionStyle.default) {
            UIAlertAction in
            exit(0);
        }
        let cancelAction = UIAlertAction(title: "Cancelar", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            alertController.dismiss(animated: true, completion: nil)
        }
        
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    @objc func pressButton(button: UIButton) {
        self.performSegue(withIdentifier: "mainToSavedGamesSegue", sender: self)
    }
    
    func save(title: String, kind:String, platform:String, price:Double, rating:Double) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let entity = NSEntityDescription.entity(forEntityName: "GameItem",
                                                in: managedContext)!
        
        let gameItem = NSManagedObject(entity: entity,
                                       insertInto: managedContext)
        
        gameItem.setValue(title, forKeyPath: "title")
        gameItem.setValue(kind, forKeyPath: "kind")
        gameItem.setValue(platform, forKeyPath: "platform")
        gameItem.setValue(price, forKeyPath: "price")
        gameItem.setValue(rating, forKeyPath: "rating")
        
        do {
            try managedContext.save()
            gameItems.append(gameItem)
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    func validationSuccessful() {
        let gameTitle = self.nameTextField.text
        let gameKind = self.kindTextField.text
        let gamePlatform = self.platformTextField.text
        let gamePrice = Double(self.priceTextField.text!)
        let gameRating = self.ratingView.rating
        
        
        self.save(title: gameTitle!, kind: gameKind!, platform: gamePlatform!, price: gamePrice!, rating: gameRating)
        
        self.nameTextField.text = ""
        self.kindTextField.text = ""
        self.platformTextField.text = ""
        self.priceTextField.text = ""
        self.ratingView.rating = 1
        
        self.nameTextField.layer.borderColor = UIColor(red:0.31, green:0.47, blue:0.69, alpha:1.0).cgColor
        self.kindTextField.layer.borderColor = UIColor(red:0.31, green:0.47, blue:0.69, alpha:1.0).cgColor
        self.platformTextField.layer.borderColor = UIColor(red:0.31, green:0.47, blue:0.69, alpha:1.0).cgColor
        self.priceTextField.layer.borderColor = UIColor(red:0.31, green:0.47, blue:0.69, alpha:1.0).cgColor
        
        let alert = UIAlertController(title: "Sucesso", message: "Seu game foi cadastrado", preferredStyle: UIAlertControllerStyle.alert)
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(defaultAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func validationFailed(_ errors: [(Validatable, ValidationError)]) {
        for (field, error) in errors {
            if let field = field as? UITextField {
                field.layer.borderColor = UIColor.red.cgColor
                field.layer.borderWidth = 1.0
            }
            error.errorLabel?.text = error.errorMessage
            error.errorLabel?.isHidden = false
            
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }

}

